## ika-driver: Credible Modelling Process
This Document is based on the Credible Modelling Process (CMP) and fills it from the perspective of the ika-driver model. The process description can be found [here](https://gitlab.sl4to5.de/deliverables/credible-simulation-process/credible-simulation-process/-/blob/0182678762e6e3f9910246913259ae5c9fa7313b/credible_simulation_process.md#introduction).

### ToDo / übergreifende Punkte:
* [ ] Dummy


# Phase 3: Design Specification

> In this process phase, the design specification for the ika-driver model task is determined from the requirements. Also, the design specification (how is it realized) to the corresponding test cases, models, simulation & test environment, and quality assurance. Important here is the documentation why the respective type of realization was determined. Conceptual implementations can be used for design specification decisions. They are separate sub tasks.
> 
> * Design specifications for test cases, models, parameters, simulation & test environment, quality assurance
> <details>
>   <summary markdown="span">Additional Information (Click)</summary>
> 
> 
> Conceptual implementations can be used for design specification decisions. They are separate sub tasks.
> With the help of conceptual implementations, implementation approaches for aspects that the model is to perform can be tested and evaluated in advance.
> They also serve to explain why the design specifications are defined in this way.
> Conceptual implementations can be:
> * Modeling approaches from literature, do not have to be implemented
> * Prototypical implementations, these can also take place in other modelling languages, tool environments (these prototype implementations can also be used afterwards for test purposes)
> Conceptual models can be used for testing aspects of the developed model.
> </details>


## Inputs

**TODO @beck_d0: Describe**
> * Overall Requirement specification for modelling task (also requirements for the overall simulation environment, including operation range, which have to be checked and tested)
> * Requirement specification test cases
> * Requirement specification models (+ parameters)
> * Requirement specification interfaces
> * Requirement specification simulation & test environment
> * Requirements according to criticality of decision of simulation task
> * Requirements specifications for process quality

**Collection of all Inputs**  


## Process Step Execution

**TODO @beck_d0: Describe**

> * Design specification for test cases, models, parameters and simulation & test environment
> * Conceptual implementations can be used for the design specification decisions
> * Documentation of reasons for design specifications
> * Design Specification of quality assurance (which methods and test sets are used to check the quality of the simulation environment in its specific operating range (which is larger than the range used in the actual tests))

**Collect doing, decisions and reasoning here**


## Outputs

**TODO @beck_d0: Describe**
> * Overall design specification for simulation setup (model, parameters, simulation & test environment)
> * Design specification model
> * Design specification parameters
> * Design specification interfaces
> * Design specification simulation & test environment
> * Design specification test cases
> * Design specification meta data
> * Design specification of quality assurance (for model, parameters, simulation & test environment and integration; can be test, review, etc.)
> * Documentation of reasons for design specifications
> * Design specification of workflow, data flow

**Collection of all Outputs**


| Previous Step | Next Step |
| ------ | ------ |
| [**Requirements Spec**](Documentation/CMP_Phase2_RequirementSpec.md) | [**Implementation**](Documentation/CMP_Phase4_Implementation.md) |
