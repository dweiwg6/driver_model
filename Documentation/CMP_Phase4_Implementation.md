## ika-driver: Credible Modelling Process
This Document is based on the Credible Modelling Process (CMP) and fills it from the perspective of the ika-driver model. The process description can be found [here](https://gitlab.sl4to5.de/deliverables/credible-simulation-process/credible-simulation-process/-/blob/0182678762e6e3f9910246913259ae5c9fa7313b/credible_simulation_process.md#introduction).

### ToDo / übergreifende Punkte:
* [ ] Dummy


# Phase 4: Implement and Assure quality for Simulation Setup

> In this process phase, the different parts of the simulation environment - test cases, models, simulation & test environment - are implemented, individually and in their interaction in the overall environment tested. A quality assurance is carried out over the specified parameter space, assured fidelity. Finally, a quality verdict about simulation setup is reached.
> 
> * Implement and test simulation models, parameters, simulation & test environment

## Inputs

**TODO @beck_d0: Describe**
> * Overall Design specification for simulation set up (models, parameters, simulation & test environment)
> * Design specification model
> * Design specification parameters
> * Design specification interfaces
> * Design specification simulation & test environment
> * Design specification test cases
> * Design specification meta data
> * Design specification of quality assurance (for model, parameters, simulation & test environment and integration; can be test, review, etc.) is part of the elements above
> * Design specification of workflow, data flow

**Collection of all Inputs**



## Process Step Execution

**TODO @beck_d0: Describe**
> * Implement simulation model
> * Implement (modules) simulation environment
> * Set up simulation environment, integrate models
> * Assure quality of structure of test & simulation environment (tests and quality-assuring methods)
> * Provision of quality information about model & simulation environment according to criticality of decision of simulation task
> * Test model and document results
> * Derive quality verdict about the model

**Collect doing, decisions and reasoning here**


## Outputs

**TODO @beck_d0: Describe**
> * Implemented, qualified and documented model, parameters, simulation & test environment according to requirement & design specifications
> * Test results according to specification
> * Quality information model & simulation environment
> * Quality verdict about model
> * Model meta data and documentation

**Collection of all Outputs**

| Previous Step | Next Step |
| ------ | ------ |
| [**Design Spec**](Documentation/CMP_Phase3_DesignSpec.md) | [**Overview**](../../tree/master) |
